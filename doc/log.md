# 日志

日志. 源码见 src/stdlib/log.c

---


```
/* 见 zlog_fatal */
extern zbool_t zvar_log_fatal_catch;

/* 见 zdebug */
extern zbool_t zvar_log_debug_enable;

/* 可自定义 zlog_vprintf, 定制日志输出 */
extern void (*zlog_vprintf) (const char *source_fn, size_t line_number, const char *fmt, va_list ap);

/* 日志输出 */
void __attribute__((format(printf,3,4))) zlog_info(const char *source_fn, size_t line_number, const char *fmt, ...);

/* 日志输出后, 进程退出, 如果 zvar_log_fatal_catch==1, 则 激活段错误 */
void __attribute__((format(printf,3,4))) zlog_fatal(const char *source_fn, size_t line_number, const char *fmt, ...);

#define zfatal(fmt, args...) { zlog_fatal(__FILE__, __LINE__, fmt, ##args); }
#define zinfo(fmt, args...) { zlog_info(__FILE__, __LINE__, fmt, ##args); }
#define zdebug(fmt,args...) { if(zvar_log_debug_enable){zinfo(fmt, ##args);} }

/* 使用syslog, identity/facility 参考 syslog  */
void zlog_use_syslog(const char *identity, int facility);

/* 转换字符串facility, 如: "LOG_MAIL" => LOG_MAIL */
int zlog_get_facility_from_str(const char *facility);

/* 使用 masterlog, master-server 提供的服务 */
/* identity: 程序名; dest: master-server提供的服务地址, domain_socket, udp */
void zlog_use_masterlog(const char *identity, const char *dest);
```
