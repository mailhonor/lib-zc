# 时间函数

时间/日期函数. 源码见 src/stdlib/time.c和src/stdlib/date.c

---


```
#define zvar_max_timeout_millisecond (3600L * 24 * 365 * 10 * 1000)

/* 返回当前毫秒精度的时间 */
long zmillisecond(void);

/* 睡眠delay毫秒 */
void zsleep_millisecond(int delay);

/* 返回 zmillisecond(void) + timeout */
long ztimeout_set_millisecond(long timeout);

/* 返回 stamp - zmillisecond(void) */
long ztimeout_left_millisecond(long stamp);

#define zvar_max_timeout (3600 * 24 * 365 * 10)

/* 返回当前秒精度的时间 */
long zsecond(void);

/* 睡眠delay秒 */
void zsleep(int delay);

/* 返回 zsecond(void) + timeout */
long ztimeout_set(int timeout);

/* 返回 stamp - zsecond(void) */
int ztimeout_left(long stamp);

#define zvar_rfc1123_date_string_size 32
/* 根据t(秒)生成rfc1123格式的时间字符串,存储在buf, 并返回, buf的长度不小于 zvar_rfc1123_date_string_size */
char *zbuild_rfc1123_date_string(long t, char *buf);

#define zvar_rfc822_date_string_size 38
/* 根据t(秒)生成rfc822格式的时间字符,存储在buf并, 返回, buf的长度不小于 zvar_rfc822_date_string_size */
char *zbuild_rfc822_date_string(long t, char *buf);

```
