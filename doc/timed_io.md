# 超时io

源码见 src/stdlib/timed\_io.c

---


```
/* 除非函数名或其他特别标注, 所有timeout单位都是秒, -1表示无限长 */

/* -1: 出错  0: 不可读写, 1: 可读写或socket异常 */
int ztimed_read_write_wait(int fd, int timeout, int *readable, int *writeable);
int ztimed_read_write_wait_millisecond(int fd, long timeout, int *readable, int *writeable);

/* -1: 出错  0: 不可读写, 1: 可读写或socket异常 */
int ztimed_read_wait_millisecond(int fd, long timeout);
int ztimed_read_wait(int fd, int timeout);

/* < 0: 出错, >0: 正常, 0: 不可读 */
int ztimed_read(int fd, void *buf, int size, int timeout);

/* 严格写size个字节, 返回值size:正常, 小于size:出错 */
int ztimed_readn(int fd, void *buf, int size, int timeout);

/* -1: 出错  0: 不可读写, 1: 可读写或socket异常 */
int ztimed_write_wait_millisecond(int fd, long timeout);
int ztimed_write_wait(int fd, int timeout);

/* 严格写size个字节, 返回值size:正常, 小于size:出错 */
int ztimed_write(int fd, const void *buf, int size, int timeout);
```
