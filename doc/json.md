## json

json 库, 序列号/反序列化. 源码见 src/json/

---

```
typedef struct zjson_t zjson_t;

#define zvar_json_type_null        0
#define zvar_json_type_bool        1
#define zvar_json_type_string      2
#define zvar_json_type_long        3
#define zvar_json_type_double      4
#define zvar_json_type_object      5
#define zvar_json_type_array       6
#define zvar_json_type_unknown     7

#pragma pack(push, 1)
struct zjson_t {
    union {
        zbool_t b;
        long l;
        double d;
        zbuf_t *s;
        zvector_t *v; /* <zjson_t *> */
        zmap_t *m; /* <char *, zjson_t *> */
    } val;
    zjson_t *parent;
    unsigned char type;
};
#pragma pack(pop)

/* 创建json */
zjson_t *zjson_create();

/* 创建undefined/null */
#define zjson_create_null zjson_create

/* 创建bool */
zjson_t *zjson_create_bool(zbool_t b);

/* long */
zjson_t *zjson_create_long(long l);

/* doube */
zjson_t *zjson_create_double(double d);

/* string */
zjson_t *zjson_create_string(const void *s, int len);

/* 释放 */
void zjson_free(zjson_t *j);

/* 重置 */
void zjson_reset(zjson_t *j);

/* 从文件加载并分析(反序列化)数据 */
zbool_t zjson_load_from_pathname(zjson_t *j, const char *pathname);

/* 反序列化数据 */
zbool_t zjson_unserialize(zjson_t *j, const char *s, int len);

/* 序列化json, 结果追加到result */
void zjson_serialize(zjson_t *j, zbuf_t *result, int strict);

/* json的类型 */
zinline int zjson_get_type(zjson_t *j) { return j->type; }
zinline zbool_t zjson_is_null(zjson_t *j)   { return j->type==zvar_json_type_null; }
zinline zbool_t zjson_is_bool(zjson_t *j)   { return j->type==zvar_json_type_bool; }
zinline zbool_t zjson_is_long(zjson_t *j)   { return j->type==zvar_json_type_long; }
zinline zbool_t zjson_is_double(zjson_t *j) { return j->type==zvar_json_type_double; }
zinline zbool_t zjson_is_string(zjson_t *j) { return j->type==zvar_json_type_string; }
zinline zbool_t zjson_is_object(zjson_t *j) { return j->type==zvar_json_type_object; }
zinline zbool_t zjson_is_array(zjson_t *j)  { return j->type==zvar_json_type_array; }

/* 获取bool值的指针; 如果不是bool类型,则首先转换为bool类型, 默认为false */
zbool_t *zjson_get_bool_value(zjson_t *j);

/* 获取long值的指针; 如果不是long类型, 则首先转换为long类型, 默认为 0 */
long *zjson_get_long_value(zjson_t *j);

/* 获取double值的指针; 如果不是double类型, 则首先转换为long类型, 默认为 0 */
double *zjson_get_double_value(zjson_t *j);

/* 获取(zbuf_t *)值的指针; 如果不是zbuf_t *类型, 则首先转换为zbuf_t *类型, 值默认为 "" */
zbuf_t **zjson_get_string_value(zjson_t *j);

/* 获取数组值的指针; 如果不是数组类型, 则首先转换为数组类型, 默认为 [] */
const zvector_t *zjson_get_array_value(zjson_t *j); /* <zjson_t *> */

/* 获取对象值的指针; 如果不是对象类型, 则首先转换为对象类型, 默认为 {} */
const zmap_t *zjson_get_object_value(zjson_t *j); /* <char *, zjson_t *> */

/* 如果不是数组,先转为数组, 获取下表为idx的 子json */
zjson_t *zjson_array_get(zjson_t *j, int idx);

/* 如果不是对象,先转为对象, 获取下键为key的i子json */
zjson_t *zjson_object_get(zjson_t *j, const char *key);

/* 如果不是数组,先转为数组, 获取数组长度 */
int zjson_array_get_len(zjson_t *j);

/* 如果不是对象,先转为对象, 获取子json个数 */
int zjson_object_get_len(zjson_t *j);

/* 如果不是数组,先转为数组, 在数组后追加element(json). 返回element */
zjson_t *zjson_array_push(zjson_t *j, zjson_t *element);
#define zjson_array_add zjson_array_push

/* 如果不是数组,先转为数组, 存在则返回1, 否则返回 0;
 * element不为0,则pop出来的json赋值给*element, 否则销毁 */
zbool_t zjson_array_pop(zjson_t *j, zjson_t **element);


/* 如果不是数组,先转为数组, 存在则返回1, 否则返回 0;
 * element不为0,则unshift出来的json赋值给*element, 否则销毁 */
zjson_t *zjson_array_unshift(zjson_t *j, zjson_t *element);

/* 如果不是数组,先转为数组, 在数组前追加element(json). 返回element */
zbool_t zjson_array_shift(zjson_t *j, zjson_t **element);

/* 已知 json = [1, {}, "ss" "aaa"]
 * 1, zjson_array_update 给键idx设置成员element. 返回element
 * 2, 如果键idx不存在, 则直接赋值
 *    2.1, 例子: zjson_array_update(json, 6, element, 0)
 *         结果: [1, {}, "ss", "aaa", null, null, 6]
 * 3, 如果键idx存在
 *    3.1, 把旧值赋值给 *old_element, 如果old_element为0,则销毁.
 *         再做element的赋值
 *    3.2, 例子: zjson_array_update_element(json, 2, element, &old_element)
 *         结果: [1, {}, element, "aaa"], 且 *old_element 为 "ss"
 *    3.3, 例子: zjson_array_update_element(json, 2, element, 0);
 *         结果: [1, {}, element, "aaa"], 且 销毁 "ss" */
zjson_t *zjson_array_update(zjson_t *j, int idx, zjson_t *element, zjson_t **old_element);

/* 把element插入idx处, idx及其后元素顺序后移 */
zjson_t *zjson_array_insert(zjson_t *j, int idx, zjson_t *element);

/* 移除idx处json,并把其值付给 *old_element, idx后元素属性前移 */
void zjson_array_delete(zjson_t *j, int idx, zjson_t **old_element);

/* 增加或更新键为key对应的json, 新值为element;
 * 旧值如果存在则赋值给*old_element, 如果old_element为了0则销毁  */
zjson_t *zjson_object_update(zjson_t *j, const char *key, zjson_t *element, zjson_t **old_element);
#define zjson_object_add zjson_object_update

/* 移除键key及对应的json;
 * json如果存在则赋值给*old_element, 如果old_element为了0则销毁  */
void zjson_object_delete(zjson_t *j, const char *key, zjson_t **old_element);

/* 已知json {group:{linux:[{}, {}, {me: {age:18, sex:"male"}}}}, 则
 * zjson_get_element_by_path(json, "group/linux/2/me") 返回的 应该是 {age:18, sex:"male"} */
zjson_t *zjson_get_element_by_path(zjson_t *j, const char *path);


/* 已知json {group:{linux:[{}, {}, {me: {age:18, sex:"male"}}}}, 则
 * zjson_get_element_by_path_vec(json, "group", "linux", "2", "me", 0); 返回的 应该是 {age:18, sex:"male"} */
zjson_t *zjson_get_element_by_path_vec(zjson_t *j, const char *path0, ...);


/* 返回父节点 */
zinline zjson_t *zjson_get_parent(zjson_t *j) { return j->parent; }

/* 返回祖先 */
zjson_t *zjson_get_top(zjson_t *j);
```

---

### 例子
见源码 sample/json/

