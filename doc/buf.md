# zbuf\_t 

字符串的封装. 源码见 src/stdlib/buf.c

---

```
struct zbuf_t {
    char *data;
    int len:31;
    unsigned int static_mode:1;
    int size:31;
    unsigned int unused_flag1:1;
}; 

/* 返回数据指针 */
#define zbuf_data(b)            ((b)->data)

/* 返回长度 */
#define zbuf_len(b)             ((b)->len)

/* 追加字节c到b, 建议使用 zbuf_put(b, c) */
#define ZBUF_PUT(b, c)  \
    (((b)->len<(b)->size)?((int)(((unsigned char *)((b)->data))[(b)->len++]=(int)(c))):(((b)->static_mode?0:zbuf_put_do((b), (c)))))

/* 创建buf, 初始容量为size */
zbuf_t *zbuf_create(int size);

/* 释放 */
void zbuf_free(zbuf_t *bf);

/* 初始化bf指向的内容为 buf, 初始容量为size */
void zbuf_init(zbuf_t *bf, int size);

/* zbuf_init 的反操作 */
void zbuf_fini(zbuf_t *bf);

/* 返回剩余容量 */
int zbuf_need_space(zbuf_t *bf, int need);

/* 追加字节ch到bf. 返回ch. 不推荐使用此函数 */
int zbuf_put_do(zbuf_t *bf, int ch);

/* 追加写字节ch到bf. 返回ch */
zinline void zbuf_put(zbuf_t *bf, int ch) { ZBUF_PUT(bf, ch); bf->data[bf->len] = 0; }

/* 重置 */
zinline void zbuf_reset(zbuf_t *bf) { bf->len=0, bf->data[0]=0; }

/* 结尾置 0 */
zinline void zbuf_terminate(zbuf_t *bf) { bf->data[bf->len]=0; }

/* 截短 */
zinline void zbuf_truncate(zbuf_t *bf, int new_len) {
    if ((bf->len>new_len) && (new_len>-1)) { bf->len=new_len; bf->data[bf->len] = 0; }
}

void zbuf_strncpy(zbuf_t *bf, const char *src, int len);
void zbuf_strcpy(zbuf_t *bf, const char *src);
void zbuf_strncat(zbuf_t *bf, const char *src, int len);
void zbuf_strcat(zbuf_t *bf, const char *src);
#define zbuf_puts zbuf_strcat
void zbuf_memcpy(zbuf_t *bf, const void *src, int len);
void zbuf_memcat(zbuf_t *bf, const void *src, int len);
zinline void zbuf_append(zbuf_t *bf, zbuf_t *bf2) { return zbuf_memcat(bf, zbuf_data(bf2), zbuf_len(bf2)); }

/* zbuf_printf_1024 意思 { char buf[1024+1], snprintf(buf, 1024, format, ...); zbuf_cat(bf, buf); } */
void zbuf_printf_1024(zbuf_t *bf, const char *format, ...);

/* 删除右侧的\r\n */
void zbuf_trim_right_rn(zbuf_t *bf);

```

---

## 例子
```
#include "zc.h"

int main(int argc, char **argv)
{
    zbuf_t *bf = zbuf_create(0);
    zbuf_put(bf, 'A');
    zbuf_strcat(bf, "aaaaaaaaaaa");
    zbuf_put(bf, 'A');
    zbuf_printf_1024(bf, "nnnew_%d_fff", 123);
    printf("%d, %s\n", zbuf_len(bf), zbuf_data(bf));
    zbuf_free(bf);
    return 0;
}
```
