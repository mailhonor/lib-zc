# zargv\_t 

argv 是 一组字符串. 源码见 src/stdlib/argv.c

---

```
typedef struct zargv_t zargv_t;
struct zargv_t {
    char **argv;
    int argc:31;
    int unused:1;
    int size:31;
    int mpool_used:1;
};
#define zargv_len(ar)           ((ar)->argc)
#define zargv_argc(ar)          ((ar)->argc)
#define zargv_argv(ar)          ((ar)->argv)
#define zargv_data(ar)          ((ar)->argv)
#define zargv_reset(ar)          (zargv_truncate((ar), 0))
#define ZARGV_WALK_BEGIN(ar, var_your_ptr)   {\
    int  zargv_tmpvar_i; const zargv_t *___ar_tmp_ptr = ar; char *var_your_ptr; \
        for(zargv_tmpvar_i=0;zargv_tmpvar_i<(___ar_tmp_ptr)->argc;zargv_tmpvar_i++){ \
            var_your_ptr = (___ar_tmp_ptr)->argv[zargv_tmpvar_i];
#define ZARGV_WALK_END                       }}

/* 创建argv, 初始容量为size */
zargv_t *zargv_create(int size);

/* 释放 */
void zargv_free(zargv_t *argvp);

/* 把strdup(ns)追加到尾部*/
void zargv_add(zargv_t *argvp, const char *ns);

/* 把strndup(ns, nlen)追加到为尾部 */
void zargv_addn(zargv_t *argvp, const char *ns, int nlen);

void zargv_truncate(zargv_t *argvp, int len);
/* 把argvp的长度截短为len */

/* 重置 */
void zargv_rest(zargv_t *argvp);

/* 用delim分割string, 并追加到argvp */
zargv_t *zargv_split_append(zargv_t *argvp, const char *string, const char *delim);

/* debug 输出 */
void zargv_debug_show(zargv_t *argvp);

```

---

## 例子

见源码 sample/stdlib/argv.c
