
# LIB-ZC 文档目录

* [简介](./introduction.md)

* [不定长字符串 buf](./buf.md)

* [容器 argv](./argv.md)

* [容器 vector](./vector.md)

* [容器 list](./list.md)

* [容器 dict, dictlong](./dict.md)

* [容器 map](./map.md)

* [数据结构 link](./link.md)

* [宏 link](./macro_link.md)

* [数据结构 rbtree](./rbtree.md)

* [宏 rbtree](./macro_rbtree.md)

* [配置, 配置文件](./config.md)

* [命令行参数](./main_argument.md)

* [日志](./log.md)

* [时间](./time.md)

* [io](./io.md)

* [超时io](./timed_io.md)

* [tcp socket](./tcp_socket.md)

* [文件](./file.md)

* [openssl](./openssl.md)

* [stream](./stream.md)

* [dns, gethostbyname, mac地址](./dns.md)

* [base64, hex, ncr, quoted\_printable](./encode.md)

* [crc64, crc32, crc16](./crc.md)

* [redis 客户端](./redis_client.md)

* [memcache 客户端](./memcache_client.md)

* [json 及其解析和序列化](./json.md)

* [字符集, 转码, 识别](./charset.md)

* [邮件解析, tnef解析](./mime.md)

* [master server/client server, 服务进程管理模型](./master.md)

* [io事件, 异步io(ssl), timer](./aio.md)

* [协程, 文件io, 锁, 条件](./coroutine.md)

* [http](./http.md)

* [const db](./cdb.md)

* [唯一id](./unique_id.md)

* [改变根,降权](./chroot_user.md)



