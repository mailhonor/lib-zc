# tcp socket

tcp 网络函数. 源码见 src/stdlib/tcp\_socket.c

---

```
#define zvar_tcp_listen_type_inet  'i'
#define zvar_tcp_listen_type_unix  'u'
#define zvar_tcp_listen_type_fifo  'f'

/* accept domain socket, 忽略EINTR */
int zunix_accept(int fd);

/* accept socket, 忽略EINTR */
int zinet_accept(int fd);

/* accept, 忽略EINTR */
int zaccept(int sock, int type);

/* listen */
int zunix_listen(char *addr, int backlog, int nonblock_flag);
int zinet_listen(const char *sip, int port, int backlog, int nonblock_flag);
int zlisten(const char *netpath, int *type, int backlog, int nonblock_flag);

int zfifo_listen(const char *path);

/* connect, 忽略EINTR */
int zunix_connect(const char *addr, int nonblock_flag, int timeout);
int zinet_connect(const char *dip, int port, int nonblock_flag, int timeout);
int zhost_connect(const char *host, int port, int nonblock_flag, int timeout);
int zconnect(const char *netpath, int nonblock_flag, int timeout);

```
