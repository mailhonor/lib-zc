## io事件, 异步io(ssl), timer

基于epoll的高并发io模型, 包括 事件, 异步io, 定时器, io映射. 源码见 src/event/aio.c

---

```
typedef struct zeio_t zeio_t;
typedef struct zaio_t zaio_t;
typedef struct zetimer_t zetimer_t;
typedef struct zevent_base_t zevent_base_t;
typedef struct ziopipe_base_t ziopipe_base_t;
```

---

```
/* eio, io可读写事件 */

/* 创建 eio, evbase是zevent_base_t */
zeio_t *zeio_create(int fd, zevent_base_t *evbase);
void zeio_free(zeio_t *eio, int close_fd);

/* 如果eio只在本线程运行, 可以执行此函数, 会稍微提高的性能 */
void zeio_set_local(zeio_t *eio);

/* 获取结果, -1: 错, 0: 无, 1: 可读/写 */
int zeio_get_result(zeio_t *eio);

/* 获取 fd */
int zeio_get_fd(zeio_t *eio);

/* 如果可读(或出错)则回调执行函数 callback */
void zeio_enable_read(zeio_t *eio, void (*callback)(zeio_t *eio));

/* 如果可写(或出错)则回调执行函数 callback */
void zeio_enable_write(zeio_t *eio, void (*callback)(zeio_t *eio));

/* 取消 zeio_enable_read/zeio_enable_write */
void zeio_disable(zeio_t *eio);

/* 设置/获取上下文 */
void zeio_set_context(zeio_t *eio, const void *ctx);
void *zeio_get_context(zeio_t *eio);

/* 返回 zevent_base_t */
zevent_base_t *zeio_get_event_base(zeio_t *eio);
```

---

```
/* aio, 异步读写io, 支持 oepnssl */

/* 创建, evbase是zevent_base_t */
zaio_t *zaio_create(int fd, zevent_base_t *evbase);
void zaio_free(zaio_t *aio, int close_fd_and_release_ssl);

/* 如果aio只在本线程运行, 可以执行此函数, 会稍微提高的性能 */
void zaio_set_local(zaio_t *aio);

/* 获取结果, 返回: -1:错, 0: 超时, >0: 可写或可读的字节数 */
int zaio_get_result(zaio_t *aio);

/* 获取 fd */
int zaio_get_fd(zaio_t *aio);

/* 获取 SSL 句柄 */
SSL *zaio_get_ssl(zaio_t *aio);

/* 发起tls连接, 成功/失败/超时后回调执行callback */
void zaio_tls_connect(zaio_t *aio, SSL_CTX * ctx, void (*callback)(zaio_t *aio), int timeout);

/* 发起tls接受, 成功/失败/超时后回调执行callback */
void zaio_tls_accept(zaio_t *aio, SSL_CTX * ctx, void (*callback)(zaio_t *aio), int timeout);

/* 从缓存中获取已读数据 */
void zaio_fetch_rbuf(zaio_t *aio, zbuf_t *bf, int strict_len);
void zaio_fetch_rbuf_data(zaio_t *aio, void *data, int strict_len);

/* 请求读, 最多读取max_len个字节, 成功/失败/超时后回调执行callback */
void zaio_read(zaio_t *aio, int max_len, void (*callback)(zaio_t *aio), int timeout);

/* 请求读, 严格读取strict_len个字节, 成功/失败/超时后回调执行callback */
void zaio_readn(zaio_t *aio, int strict_len, void (*callback)(zaio_t *aio), int timeout);

/* */
void zaio_read_size_data(zaio_t *aio, void (*callback)(zaio_t *aio), int timeout);

/* 请求读, 读到delimiter为止, 最多读取max_len个字节, 成功/失败/超时后回调执行callback */
void zaio_gets_delimiter(zaio_t *aio, int delimiter, int max_len, void (*callback)(zaio_t *aio), int timeout);

/* 如上, 读行 */
void zaio_gets(zaio_t *aio, int max_len, void (*callback)(zaio_t *aio), int timeout);

/* 向缓存写数据, (fmt, ...)不能超过1024个字节 */
void zaio_cache_printf_1024(zaio_t *aio, const char *fmt, ...);

/* 向缓存写数据 */
void zaio_cache_puts(zaio_t *aio, const char *s);

/* 向缓存写数据 */
void zaio_cache_write(zaio_t *aio, const void *buf, int len);

/* */
void zaio_cache_write_size_data(zaio_t *aio, const void *buf, int len);

/* 向缓存写数据, 不复制buf */
void zaio_cache_write_direct(zaio_t *aio, const void *buf, int len);

/* 请求写, 成功/失败/超时后回调执行callback */
void zaio_cache_flush(zaio_t *aio, void (*callback)(zaio_t *aio), int timeout);

/* */
int zaio_get_cache_size(zaio_t *aio);

/* 请求sleep, sleep秒后回调执行callback */
void zaio_sleep(zaio_t *aio, void (*callback)(zaio_t *aio), int timeout);

/* 设置/获取上下文 */
void zaio_set_context(zaio_t *aio, const void *ctx);
void *zaio_get_context(zaio_t *aio);

/* 获取 zevent_base_t */
zevent_base_t *zaio_get_event_base(zaio_t *aio);

/* */
void zaio_list_append(zaio_t **list_head, zaio_t **list_tail, zaio_t *aio);
void zaio_list_detach(zaio_t **list_head, zaio_t **list_tail, zaio_t *aio);
```

---

```
/* etimer, 基于zevent_base_t的定时器 */

/* 创建定时器 */
zetimer_t *zetimer_create(zevent_base_t *evbase);
void zetimer_free(zetimer_t *et);

/* 启动定时器, timeout秒后停止定时器,且回调执行callback */
void zetimer_start(zetimer_t *et, void (*callback)(zetimer_t *et), int timeout);

/* 停止定时器 */
void zetimer_stop(zetimer_t *et);

/* 如果etimer只在本线程运行, 可以执行此函数, 会稍微提高的性能 */
void zetimer_set_local(zetimer_t *et);

/* 设置上下文 */
void zetimer_set_context(zetimer_t *et, const void *ctx);

/* 设置/获取上下文 */
void *zetimer_get_context(zetimer_t *et);
zevent_base_t *zetimer_get_event_base(zetimer_t *et);
```

---

```
/* event/epoll 运行框架 */

/* 默认event_base */
extern zevent_base_t *zvar_default_event_base;

/* 创建 event_base */
zevent_base_t *zevent_base_create();
void zevent_base_free(zevent_base_t *eb);

/* 运行 event_base */
void zevent_base_run(zevent_base_t *eb, void (*loop_fn)());

/* 通知 event_base 停止, 既 zevent_base_run 返回 */
void zevent_base_stop_notify(zevent_base_t *eb);

/* 通知 event_base, 手动打断 epoll_wait */
void zevent_base_event_notify(zevent_base_t *eb);

/* 如果event_base只在本线程运行, 可以执行此函数, 会稍微提高的性能 */
void zevent_base_set_local(zevent_base_t *eb);

/* 设置/获取上下文 */
void zevent_base_set_context(zevent_base_t *eb, const void *ctx);
void *zevent_base_get_context(zevent_base_t *eb);
```

---

```
/* io映射, 支持 SSL, 源码见 src/event/iopipe.c  */

typedef void (*ziopipe_after_close_fn_t) (void *);

/* 创建iopipe_base */
ziopipe_base_t *ziopipe_base_create();
void ziopipe_base_free(ziopipe_base_t *iopb);

/* 获取iopipe的个数 */
int ziopipe_base_get_count(ziopipe_base_t *iopb);

/* 通知iopipe_base, 手动打断epoll_wait */
void ziopipe_base_stop_notify(ziopipe_base_t *iopb);

/* 设置超时时间, iopipe两端端口, 其中一个端口被动断开后, 另个一个端口最长timeout秒后断开 */
void ziopipe_base_after_peer_closed_timeout(ziopipe_base_t *iopb, int timeout);

/* 一次事件分发, 然后, 返回 */
void ziopipe_base_dispatch(ziopipe_base_t *iopb);

/* 进入iopipe; after_close: 两端端口关闭后 执行回调函数 after_close, 参数为 context */
void ziopipe_enter(ziopipe_base_t * iopb, int client_fd, SSL *client_ssl, int server_fd, SSL *server_ssl, ziopipe_after_close_fn_t after_close, const void *context);

```

---

### 例子
见源码 sample/event/
