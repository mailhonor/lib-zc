# stream, io高级流

io流/openssl流封装. 源码见 src/stream/

---


~~~
typedef struct zstream_t zstream_t;
struct zstream_t {
    short int read_buf_p1;
    short int read_buf_p2;
    short int write_buf_len;
    unsigned short int error:1;
    unsigned short int eof:1;
    unsigned short int ssl_mode:1;
    unsigned short int file_mode:1;
    unsigned char read_buf[zvar_stream_rbuf_size];
    unsigned char write_buf[zvar_stream_wbuf_size];
    long cutoff_time;
    union { int fd; SSL *ssl; } ioctx;
};

/* 宏, 返回读取的下一个字符, -1:错 */
#define ZSTREAM_GETC(fp) (((fp)->read_buf_p1<(fp)->read_buf_p2)?((int)((fp)->read_buf[(fp)->read_buf_p1++])):(zstream_getc_do(fp)))

/* 宏, 写一个字符ch到fp, -1:错 */
#define ZSTREAM_PUTC(fp, ch) (((fp)->write_buf_len<zvar_stream_wbuf_size)?((fp)->write_buf[(fp)->write_buf_len++]=(int)(ch),(int)(ch)):(zstream_putc_do(fp, ch)))

/* 宏, 是否出错 */
#define zstream_is_error(fp)        ((fp)->error)

/* 宏, 是否读到结尾 */
#define zstream_is_eof(fp)          ((fp)->eof)

/* 宏, 是否异常(错或读到结尾) */
#define zstream_is_exception(fp)    ((fp)->eof||(fp)->error)

/* 宏, 可读缓存的长度 */
#define zstream_get_read_cache_len(fp) ((fp)->read_buf_p2-(fp)->read_buf_p1)

/* 基于文件描述符fd创建stream */
zstream_t *zstream_open_fd(int fd);

/* 基于ssl创建stream */ 
zstream_t *zstream_open_ssl(SSL *ssl);

/* 打开本地文件, mode: "r", "r+", "w", "w+", "a", "a+" */
zstream_t *zstream_open_file(const char *pathname, const char *mode);

/* 打开地址destination, timeout:是超时, 单位秒. destination: 见 zconnect */
zstream_t *zstream_open_destination(const char *destination, int timeout);

/* 关闭stream, close_fd_and_release_ssl: 是否同时关闭相关fd */
int zstream_close(zstream_t *fp, zbool_t close_fd_and_release_ssl);

/* 返回 fd */
int zstream_get_fd(zstream_t *fp);

/* 返回ssl */
SSL *zstream_get_ssl(zstream_t *fp);

/* 发起tls_connect, -1:错, 0:成功 */
int zstream_tls_connect(zstream_t *fp, SSL_CTX *ctx);

/* 发起tls_accept, -1:错, 0:成功 */
int zstream_tls_accept(zstream_t *fp, SSL_CTX *ctx);

/* 设置超时, 单位秒 */
void zstream_set_timeout(zstream_t *fp, int timeout);

/* 超时等待可读, 单位秒, timeout=-1: 表示无限长 */
int zstream_timed_read_wait(zstream_t *fp, int timeout);

/* 超时等待可写, 单位秒 */
int zstream_timed_write_wait(zstream_t *fp, int timeout);

/* 读取一个字符, -1: 错误 */
/* 不应该使用这个函数 */
int zstream_getc_do(zstream_t *fp);

/* 读取一个字符, -1: 错误 */
zinline int zstream_getc(zstream_t *fp) { return ZSTREAM_GETC(fp); }

/* 使用条件太苛刻, 不推荐使用 */
void zstream_ungetc(zstream_t *fp);

/* 读 max_len个字节到bf, -1: 错, 0: 不可读, 1: 读取字节数 */
int zstream_read(zstream_t *fp, zbuf_t *bf, int max_len);

/* 严格读取strict_len个字符 */
int zstream_readn(zstream_t *fp, zbuf_t *bf, int strict_len);

/* 读取最多max_len个字符到bf, 读取到delimiter为止 */
int zstream_gets_delimiter(zstream_t *fp, zbuf_t *bf, int delimiter, int max_len);

/* 读取一行 */
zinline int zstream_gets(zstream_t *fp, zbuf_t *bf, int max_len)
{
    return zstream_gets_delimiter(fp, bf, '\n', max_len);
}

int zstream_size_data_get_size(zstream_t *fp);

/* 写一个字节ch, 返回-1:错, 返回ch:成功 */
/* 不推荐使用 */
int zstream_putc_do(zstream_t *fp, int ch);

/* 写一个字节ch, 返回-1:错, 返回ch:成功 */
zinline int zstream_putc(zstream_t *fp, int c) { return ZSTREAM_PUTC(fp, c); }

/* 写程度为len的buf到fp, 返回-1:失败, 其他:成功 */
int zstream_write(zstream_t *fp, const void *buf, int len);

/* 写一行s 到fp, 返回-1:失败, 其他:成功 */
int zstream_puts(zstream_t *fp, const char *s);

#define zstream_puts_const(fp, s) zstream_write(fp, s, sizeof(s)-1)

/* 写bf到fp, 返回-1:失败, 其他:成功 */
zinline int zstream_append(zstream_t *fp, zbuf_t *bf) {
    return zstream_write(fp, zbuf_data(bf), zbuf_len(bf));
}

/* zstream_printf_1024, 意思是:
 * char buf[1024+1]; sprintf(buf, format, ...); zstream_puts(fp, buf); */
int zstream_printf_1024(zstream_t *fp, const char *format, ...);

int zstream_write_size_data_size(zstream_t *fp, int len);
int zstream_write_size_data(zstream_t *fp, const void *buf, int len);
int zstream_write_size_data_int(zstream_t *fp, int i);
int zstream_write_size_data_long(zstream_t *fp, long i);
int zstream_write_size_data_dict(zstream_t *fp, zdict_t * zd);
int zstream_write_size_data_pp(zstream_t *fp, const char **pp, int size);

/* flush, 返回-1:错 */
int zstream_flush(zstream_t *fp);
~~~


### 例子

见源码 sample/stream/io.c
